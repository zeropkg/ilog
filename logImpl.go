package ilog

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

const (
	level = "level"
	msg   = "msg"
)

type LogImpl struct {
	fields map[string]interface{}
}

func NewLogImpl(fields ...map[string]interface{}) *LogImpl {
	if len(fields) == 0 {
		return &LogImpl{}
	}
	log.SetFlags(0)
	return &LogImpl{fields: fields[0]}
}

func (this *LogImpl) mergeFields(params map[string]interface{}) map[string]interface{} {
	if this.fields == nil || len(this.fields) == 0 {
		return params
	}
	// 要考虑params和fields谁大谁小
	var result map[string]interface{}
	if len(this.fields) < len(params) {
		result = params
		for k, v := range this.fields {
			result[k] = v
		}
	} else {
		result = this.fields
		for k, v := range params {
			result[k] = v
		}
	}
	return result
}

// jsonPrint 打印json格式的日志
func (this *LogImpl) jsonPrint(params map[string]interface{}) {
	b, err := json.Marshal(this.mergeFields(params))
	if err != nil {
		return
	}
	log.Printf("%s", b)
}

func (this *LogImpl) Info(args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelInfo,
		msg:   fmt.Sprint(args...),
	})
}

func (this *LogImpl) Warn(args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelWarn,
		msg:   fmt.Sprint(args...),
	})
}

func (this *LogImpl) Error(args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelError,
		msg:   fmt.Sprint(args...),
	})
}

func (this *LogImpl) Fatal(args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelFatal,
		msg:   fmt.Sprint(args...),
	})
	os.Exit(0)
}

func (this *LogImpl) Infof(format string, args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelInfo,
		msg:   fmt.Sprintf(format, args...),
	})
}

func (this *LogImpl) Warnf(format string, args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelWarn,
		msg:   fmt.Sprintf(format, args...),
	})
}

func (this *LogImpl) Errorf(format string, args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelError,
		msg:   fmt.Sprintf(format, args...),
	})
}
func (this *LogImpl) Fatalf(format string, args ...interface{}) {
	this.jsonPrint(map[string]interface{}{
		level: logLevelFatal,
		msg:   fmt.Sprintf(format, args...),
	})
	os.Exit(0)
}

// InfoWithField 注意， 这里没有f
func (this *LogImpl) InfoWithField(logField map[string]interface{}, args ...interface{}) {
	p := map[string]interface{}{
		level: logLevelInfo,
		msg:   fmt.Sprint(args...),
	}

	for k, v := range logField {
		p[k] = v
	}

	this.jsonPrint(p)
}

// InfofWithField 注意， 这里有f
func (l *LogImpl) InfofWithField(logField map[string]interface{}, format string, args ...interface{}) {
	p := map[string]interface{}{
		level: logLevelInfo,
		msg:   fmt.Sprintf(format, args...),
	}

	for k, v := range logField {
		p[k] = v
	}

	l.jsonPrint(p)
}
