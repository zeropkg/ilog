package ilog

import (
	"log"
	"testing"
)

func TestNewLogerManager(t *testing.T) {
	fields := map[string]interface{}{"a": 1, "b": "2"}
	lm := NewLogerManager(fields)
	lm.Info("test")
}

func BenchmarkInfo(b *testing.B) {
	fields := map[string]interface{}{"a": 1, "b": "2"}
	lm := NewLogerManager(fields)
	var count int
	for i := 0; i < b.N; i++ {
		lm.Info("test")
		count++
	}
	log.Println(count)
}
