package ilog

type LogManager struct {
	enable bool
	ILog
}

func NewLogerManager(fields ...map[string]interface{}) *LogManager {
	if len(fields) == 0 {
		return &LogManager{enable: true, ILog: NewLogImpl()}
	}
	return &LogManager{
		enable: true,
		ILog:   NewLogImpl(fields[0]),
	}
}

func (m *LogManager) SetLoger(l ILog) {
	m.ILog = l
}
func (m *LogManager) Enable(isEnable bool) {
	m.enable = isEnable
}

func (m *LogManager) printLog(logFunc func(...interface{}), args ...interface{}) {
	if !m.enable {
		return
	}
	if logFunc == nil {
		return
	}
	logFunc(args...)
}

func (m *LogManager) printLogWithField(logFunc func(map[string]interface{}, ...interface{}), logField map[string]interface{}, args ...interface{}) {
	if !m.enable {
		return
	}

	if logFunc == nil {
		return
	}

	logFunc(logField, args...)
}

func (m *LogManager) printfLog(logFunc func(string, ...interface{}), format string, args ...interface{}) {
	if !m.enable {
		return
	}
	if logFunc == nil {
		return
	}
	logFunc(format, args...)
}

func (m *LogManager) printfLogWithField(logFunc func(map[string]interface{}, string, ...interface{}), logField map[string]interface{}, format string, args ...interface{}) {
	if !m.enable {
		return
	}

	if logFunc == nil {
		return
	}

	logFunc(logField, format, args...)
}

func (m *LogManager) Info(args ...interface{}) {
	m.printLog(m.ILog.Info, args...)
}
func (m *LogManager) InfoWithField(logField map[string]interface{}, args ...interface{}) {
	m.printLogWithField(m.ILog.InfoWithField, logField, args...)
}

func (m *LogManager) Warn(args ...interface{}) {
	m.printLog(m.ILog.Warn, args...)
}
func (m *LogManager) Error(args ...interface{}) {
	m.printLog(m.ILog.Error, args...)
}
func (m *LogManager) Fatal(args ...interface{}) {
	m.printLog(m.ILog.Fatal, args...)
}

func (m *LogManager) Infof(format string, args ...interface{}) {
	m.printfLog(m.ILog.Infof, format, args...)
}
func (m *LogManager) InfofWithField(logField map[string]interface{}, format string, args ...interface{}) {
	m.printfLogWithField(m.ILog.InfofWithField, logField, format, args...)
}
func (m *LogManager) Warnf(format string, args ...interface{}) {
	m.printfLog(m.ILog.Warnf, format, args...)
}
func (m *LogManager) Errorf(format string, args ...interface{}) {
	m.printfLog(m.ILog.Errorf, format, args...)
}
func (m *LogManager) Fatalf(format string, args ...interface{}) {
	m.printfLog(m.ILog.Fatalf, format, args...)
}
